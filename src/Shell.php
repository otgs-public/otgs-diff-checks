<?php
/**
 * @author OnTheGo Systems
 */


namespace OTGS\DiffChecks;


class Shell {
	private $output;

	public function __construct( Output $output ) {
		$this->output = $output;
	}

	/**
	 * @param string $command
	 * @param bool   $stop_on_error
	 *
	 * @return \OTGS\DiffChecks\Shell\Result
	 */
	public function exec( $command, $stop_on_error = false ) {
		exec( $command, $output, $return_var );

		if ( $stop_on_error && $return_var !== 0 ) {
			if ( $return_var === 127 ) {
				$this->output->error( 'Command not found!', [ $command ] );
			} else {
				$this->output->error( $output );
			}
			$this->stop( $return_var );
		}

		return new \OTGS\DiffChecks\Shell\Result( $command, $output, $return_var );
	}

	/**
	 * @param int $code
	 */
	public function stop( $code ) {
		exit( $code );
	}

	/**
	 * @param string $path
	 * @param string $content
	 *
	 * @return bool|int @see file_put_contents
	 */
	public function save_file( $path, $content ) {
		return file_put_contents( $path, $content );
	}

	/**
	 * @param string $path
	 *
	 * @return string $content
	 */
	public function read_file( $path ) {
		return file_get_contents( $path );
	}

	/**
	 * @param string $varName
	 * @param bool   $localOnly
	 *
	 * @return string $varValue
	 */
	public function get_env( $varName, $localOnly = false ) {
		return getenv( $varName, $localOnly );
	}
}
