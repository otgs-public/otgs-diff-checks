<?php
/**
 * @author OnTheGo Systems
 */


namespace OTGS\DiffChecks;


class App {
	private $output;
	private $shell;
	private $git;
	private $target_branch;
	private $source_branch;

	private $php_files = [];
	private $diff_file = null;

	public function __construct(
		Output $output,
		Shell $shell,
		GIT $git,
		$source_branch,
		$target_branch
	) {
		$this->output        = $output;
		$this->shell         = $shell;
		$this->git           = $git;
		$this->target_branch = $target_branch;
		$this->source_branch = $source_branch;
	}

	public function run() {
		$this->check_version();

		$this->output->head( 'Running checks on modified files' );
		$this->output->message( '- Target branch: ' . $this->target_branch );
		$this->output->message( '- Source branch: ' . $this->source_branch );

		if ( ! $this->init_changed_files() ) {
			$this->output->info( 'Done: no changed files to check!' );
			$this->shell->stop( 0 );
		}

		$this->output->info( 'Found ' . count( $this->php_files)  . ' changed file(s):' );
		array_map(
			function ( $file, $index ) {
				$this->output->info( '  ' . ( $index + 1 ) . '. ' . $file );
			},
			$this->php_files,
			array_keys( $this->php_files )
		);

		$this->diff_file = $this->build_diff_file();

		$checks = new Checks( $this->output, $this->shell );

		$this->add_commands( $checks );

		$this->output->head( 'Running commands' );
		$checks->run_commands( true );
	}

	private function check_version() {
		$is_CI            = (bool) $this->shell->get_env( 'CI' );
		$is_CI_COMMIT_TAG = $this->shell->get_env( 'CI_COMMIT_TAG' );
		if ( ! $is_CI || $is_CI_COMMIT_TAG ) {
			$lastTag = $this->git->getLastTag();
			if ( $is_CI_COMMIT_TAG ) {
				$lastTag = $is_CI_COMMIT_TAG;
			}

			$this->output->head( 'Running checks on plugin version vs latest tag' );
			$plugins = new \OTGS\DiffChecks\WP\Plugins( getcwd(), $this->shell, $this->output );
			$plugins->checkVersion( $lastTag );
		}
	}

	private function init_changed_files() {
		$this->output->head( 'Retrieving modified files' );

		$file_extensions = [ '.php' ];
		$this->php_files = $this->git->get_files( $this->target_branch, $file_extensions );

		return $this->php_files;
	}

	/**
	 * @return string
	 */
	private function build_diff_file() {
		$this->output->head( 'Building diff file' );
		$this->diff_file = $this->git->build_diff( $this->target_branch );

		return $this->diff_file;
	}

	/**
	 * @param \OTGS\DiffChecks\Checks $checks
	 */
	private function add_commands( Checks $checks ) {
		$php_fixer_tip = [
			'Run PHP Code Beautifier and Fixer for automatic fix (where applicable) or fix the issues manually:',
			'./vendor/bin/phpcbf ' . implode( ' ', $this->php_files ),
		];

		$checks->add_command( 'phpcpd', [
			'title'   => 'Code duplication',
			'command' => 'vendor/bin/phpcpd --exclude tests --exclude vendor -- ' . implode( ' ', $this->php_files ),
		] );

		$checks->add_command( 'php-lint', [
			'per_file' => true,
			'title'    => 'Syntax check',
			'command'  => array_reduce( $this->php_files, function ( $carry, $php_file ) {
				return $carry . 'php -l -d display_errors=0 ' . $php_file . PHP_EOL;
			}, '' ),
		] );

		$checks->add_command( 'phpcs', [
			'filter'     => 'phpcsStrict',
			'diff_file'  => $this->diff_file,
			'title'      => 'Coding standards',
			'command'    => './vendor/bin/phpcs --standard=./phpcs.xml --report=json ' . implode( ' ', $this->php_files ),
			'error_tips' => $php_fixer_tip,
		] );

		$checks->add_command( 'phpcs.compatibility', [
			'filter'     => 'phpcsStrict',
			'diff_file'  => $this->diff_file,
			'title'      => 'PHP Compatibility',
			'command'    => './vendor/bin/phpcs --standard=./phpcs.compatibility.xml --report=json ' . implode( ' ', $this->php_files ),
			'error_tips' => $php_fixer_tip,
		] );
	}
}
