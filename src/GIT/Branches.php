<?php
/**
 * @author OnTheGo Systems
 */


namespace OTGS\DiffChecks\GIT;


use OTGS\DiffChecks\GIT;

class Branches {

	/**
	 * @var string
	 */
	private $target_branch;
	/**
	 * @var string
	 */
	private $source_branch;
	/**
	 * @var \OTGS\DiffChecks\GIT
	 */
	private $git;

	/**
	 * Branches constructor.
	 *
	 * @param \OTGS\DiffChecks\GIT $git
	 */
	public function __construct( GIT $git ) {
		$this->git = $git;
		$this->init();
	}

	private function init() {
		$this->target_branch = 'develop';
		if ( getenv( 'CI_MERGE_REQUEST_TARGET_BRANCH_NAME' ) ) {
			$this->target_branch = getenv( 'CI_MERGE_REQUEST_TARGET_BRANCH_NAME' );
		}

		$this->source_branch = $this->git->get_current_branch();
		if ( getenv( 'CI_MERGE_REQUEST_SOURCE_BRANCH_NAME' ) ) {
			$this->source_branch = getenv( 'CI_MERGE_REQUEST_SOURCE_BRANCH_NAME' );
		}
	}

	public function get_source() {
		return $this->source_branch;
	}

	public function get_target() {
		return $this->target_branch;
	}
}
