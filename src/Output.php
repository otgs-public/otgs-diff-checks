<?php

namespace OTGS\DiffChecks;

class Output {
	const COLORS_ERROR_F = 'red';
	const COLORS_ERROR_B = null;

	const COLORS_WARNING_F = 'yellow';
	const COLORS_WARNING_B = null;

	const COLORS_INFO_F = 'green';
	const COLORS_INFO_B = null;

	private $foreground_colors;
	private $background_colors;

	/**
	 * Output constructor.
	 */
	public function __construct() {
		$this->foreground_colors['black']        = [ '0', '30' ];
		$this->foreground_colors['dark_gray']    = [ '1', '30' ];
		$this->foreground_colors['blue']         = [ '0', '34' ];
		$this->foreground_colors['light_blue']   = [ '1', '34' ];
		$this->foreground_colors['green']        = [ '0', '32' ];
		$this->foreground_colors['light_green']  = [ '1', '32' ];
		$this->foreground_colors['cyan']         = [ '0', '36' ];
		$this->foreground_colors['light_cyan']   = [ '1', '36' ];
		$this->foreground_colors['red']          = [ '0', '31' ];
		$this->foreground_colors['light_red']    = [ '1', '31' ];
		$this->foreground_colors['purple']       = [ '0', '35' ];
		$this->foreground_colors['light_purple'] = [ '1', '35' ];
		$this->foreground_colors['brown']        = [ '0', '33' ];
		$this->foreground_colors['yellow']       = [ '1', '33' ];
		$this->foreground_colors['light_gray']   = [ '0', '37' ];
		$this->foreground_colors['white']        = [ '1', '37' ];

		$this->background_colors['black']      = [ '40' ];
		$this->background_colors['red']        = [ '41' ];
		$this->background_colors['green']      = [ '42' ];
		$this->background_colors['yellow']     = [ '43' ];
		$this->background_colors['blue']       = [ '44' ];
		$this->background_colors['magenta']    = [ '45' ];
		$this->background_colors['cyan']       = [ '46' ];
		$this->background_colors['light_gray'] = [ '47' ];
	}

	function head( $title ) {
		$this->message( ':: ' . $title  );
	}

	function message( $text, $data = null, $foreground = null, $background = null ) {
		echo $this->colorize( $text, $foreground, $background ) . PHP_EOL;
		if ( $data ) {
			echo $this->colorize( print_r( $data, true ), $foreground, $background ) . PHP_EOL;
		}
	}

	function colorize( $message, $foreground = null, $background = null ) {
		$color_code  = '';
		$reset_color = '';
		if ( $foreground || $background ) {
			$color_codes = [];

			$this->get_foreground_color( $foreground ) && array_push( $color_codes, ...$this->get_foreground_color( $foreground ) );
			$this->get_background_color( $background ) && array_push( $color_codes, ...$this->get_background_color( $background ) );

			$color_code .= "\e[" . implode( ';', $color_codes ) . 'm';

			$reset_color = $this->get_reset_color();
		}

		return $color_code
			   . $message
			   . $reset_color;
	}

	private function get_foreground_color( $foreground ) {
		$code = [];
		if ( array_key_exists( $foreground, $this->foreground_colors ) ) {
			$code = $this->foreground_colors[ $foreground ];
		}

		return $code;
	}

	private function get_background_color( $background ) {
		$code = [];
		if ( array_key_exists( $background, $this->background_colors ) ) {
			$code = $this->background_colors[ $background ];
		}

		return $code;
	}

	private function get_reset_color() {
		return "\e[0m";
	}

	function head_error( $title ) {
		$this->error(  ':: ' . $title  );
	}

	function error( $message, $data = null ) {
		$this->message( $message, $data, self::COLORS_ERROR_F, self::COLORS_ERROR_B );
	}

	function subhead( $title ) {
		$this->message( '.: ' . $title );
	}

	function subhea_error( $title ) {
		$this->error( '.: ' . $title );
	}

	function warning( $message, $data = null ) {
		$this->message( $message, $data, self::COLORS_WARNING_F, self::COLORS_WARNING_B );
	}

	function info( $message, $data = null ) {
		$this->message( $message, $data, self::COLORS_INFO_F, self::COLORS_INFO_B );

	}
}
