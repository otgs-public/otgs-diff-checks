<?php

namespace OTGS\DiffChecks;

class Checks {
	/**
	 * @var \OTGS\DiffChecks\Output
	 */
	private $output;
	/**
	 * @var \OTGS\DiffChecks\Shell
	 */
	private $shell;

	private $commands = [];

	public function __construct( Output $output, Shell $shell ) {
		$this->output = $output;
		$this->shell  = $shell;
	}

	public function add_command( $id, array $command_definition ) {
		$this->commands[ $id ] = $command_definition;
	}

	function run_commands( $with_output = false ) {
		$success  = [];
		$failures = [];

		foreach ( $this->commands as $id => $command_definition ) {
			$defaults = [
				'per_file'   => false,
				'use_diff'   => false,
				'filter'     => null,
				'diff_file'  => null,
				'error_tips' => [],
			];

			$command_definition = array_merge( $defaults, $command_definition );

			$command = trim( $command_definition['command'] );
			if ( $command ) {
				if ( $this->run_command( $command, $id, $command_definition, $output ) ) {
					$success[ $command_definition['title'] ] = $output;
				} else {
					$output_message = implode( PHP_EOL, $output ) . PHP_EOL;
					if ( $command_definition['error_tips'] ) {
						$output_message .= PHP_EOL . implode( PHP_EOL, $command_definition['error_tips'] ) . PHP_EOL;
					}
					$failures[ $command_definition['title'] ] = $output_message;
				}
			}
		}

		$data = [
			'success'  => $success,
			'failures' => $failures,
		];

		if ( $with_output ) {
			$this->output_results( $data );
		}

		return $data;
	}

	function run_command( $command, $id, $command_definition, &$output ) {
		$output = [];
		$result = $this->shell->exec( $command );

		$success = 0 === $result->return_var;
		$output  = $result->output;

		if ( ! $success && $command_definition['diff_file'] && $command_definition['filter'] ) {
			$this->shell->save_file( '/tmp/' . $id . '.json', implode( PHP_EOL, $result->output ) );
			$output = [];

			$diff_filter      = './vendor/bin/diffFilter --' . $command_definition['filter'] . ' ' . $command_definition['diff_file'] . ' /tmp/' . $id . '.json 100';
			$diffFilterResult = $this->shell->exec( $diff_filter );

			$success = 0 === $diffFilterResult->return_var;
			$output  = $diffFilterResult->output;
		}

		return $success;
	}

	function output_results( $data ) {
		if ( $data['success'] ) {
			foreach ( $data['success'] as $id => $output ) {
				$this->output->info( '- ' . $id . ': OK!' );
			}
		}

		if ( $data['failures'] ) {
			foreach ( $data['failures'] as $id => $output ) {
				$this->output->error( '- ' . $id . ': Failed!' );
			}
			echo PHP_EOL;
		}

		if ( $data['failures'] ) {
			foreach ( $data['failures'] as $id => $output ) {
				$this->output->head_error( $id );
				$this->output->error( $output );
			}
			exit( 100 );
		}
		$this->output->info( 'No problems found!' );
	}
}
