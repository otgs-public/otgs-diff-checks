<?php
/**
 * @author OnTheGo Systems
 */


namespace OTGS\DiffChecks\WP;


class Plugins {
	private $current_file_content;
	private $cwd;
	/**
	 * @var \OTGS\DiffChecks\Shell
	 */
	private $shell;
	/**
	 * @var array
	 */
	private $predefinedPatterns;
	/**
	 * @var \OTGS\DiffChecks\Output
	 */
	private $output;

	/**
	 * Release_Notes_Link constructor.
	 *
	 * @param string                  $path
	 * @param \OTGS\DiffChecks\Shell  $shell
	 * @param \OTGS\DiffChecks\Output $output
	 */
	public function __construct( $path, \OTGS\DiffChecks\Shell $shell, \OTGS\DiffChecks\Output $output ) {
		$this->cwd                = $path;
		$this->shell              = $shell;
		$this->predefinedPatterns = [
			[
				'searchPattern' => '(Version:\\s*)(\\d*.*)',
				'matchingGroup' => 2,
			],
			[
				'searchPattern' => '(ICL_SITEPRESS_VERSION\\\',\\s*\\\')(\\d*.*)(\\\')',
				'matchingGroup' => 2,
			],
			[
				'searchPattern' => '(GRAVITYFORMS_MULTILINGUAL_VERSION\\\',\\s*\\\')(\\d*.*)(\\\')',
				'matchingGroup' => 2,
			],
			[
				'searchPattern' => '(WCML_VERSION\\\',\\s*\\\')(\\d*.*)(\\\')',
				'matchingGroup' => 2,
			],
			[
				'searchPattern' => '(WPML_CMS_NAV_VERSION\\\',\\s*\\\')(\\d*.*)(\\\')',
				'matchingGroup' => 2,
			],
			[
				'searchPattern' => '(WPML_MEDIA_VERSION\\\',\\s*\\\')(\\d*.*)(\\\')',
				'matchingGroup' => 2,
			],
			[
				'searchPattern' => '(WPML_ST_VERSION\\\',\\s*\\\')(\\d*.*)(\\\')',
				'matchingGroup' => 2,
			],
			[
				'searchPattern' => '(WPML_STICKY_LINKS_VERSION\\\',\\s*\\\')(\\d*.*)(\\\')',
				'matchingGroup' => 2,
			],
			[
				'searchPattern' => '(WPML_TM_VERSION\\\',\\s*\\\')(\\d*.*)(\\\')',
				'matchingGroup' => 2,
			],
			[
				'searchPattern' => '(CF7ML_VERSION\\\',\\s*\\\')(\\d*.*)(\\\')',
				'matchingGroup' => 2,
			],
		];
		$this->output             = $output;
	}

	public function checkVersion( $validVersion, $patterns = [] ) {
		if ( ! $patterns ) {
			$patterns = $this->getPatterns();
		}

		$sanitizedValidVersion = $this->extractSemVer( $validVersion );
		if ( $sanitizedValidVersion !== $validVersion ) {
			$this->output->subhead( 'Expected version: ' . $sanitizedValidVersion . ' (sanitized from ' . $validVersion . ')' );
			$validVersion = $sanitizedValidVersion;
		} else {
			$this->output->subhead( 'Expected version: ' . $validVersion );
		}
		$versions = [];


		$main_plugin = $this->getMainPluginFile();

		if ( $main_plugin ) {
			$file         = $main_plugin['file'];
			$file_content = $main_plugin['content'];

			/** @var RegExArguments $regex_args */
			foreach ( $patterns as $index => $regex_args ) {
				preg_match( '/' . $regex_args['searchPattern'] . '/i', $file_content, $matches );

				$matchingGroup = $regex_args['matchingGroup'] === 'last' ? ( count( $matches ) - 1 ) : $regex_args['matchingGroup'];
				if ( count( $matches ) >= $matchingGroup ) {
					$versions[ $regex_args['searchPattern'] ] = $matches[ $matchingGroup ];
				}
			}

			if ( $versions ) {
				$failingVersions = array_filter( $versions, function ( $version ) use ( $validVersion ) {
					$sanitizedVersion = $this->extractSemVer( $version );

					return version_compare( $sanitizedVersion, $validVersion, '<' );
				} );

				if ( $failingVersions ) {
					$this->output->error( 'Problems in ' . $file . ':' );
					array_walk( $failingVersions, function ( $match, $pattern ) use ( $validVersion ) {
						$this->output->error( '- ' . $match . ' found with ' . $pattern . ': expected ' . $validVersion );
					} );
					$this->shell->stop( 100 );
				}
			}
		}


		$this->output->info( 'Versions matching: Ok' );
	}

	private function getPatterns() {
		$patterns = $this->predefinedPatterns;
		if ( $this->shell->get_env( 'OTGS_CHECKS_VERSION_PATTERNS' ) ) {
			$new_patterns = json_decode( $this->shell->get_env( 'OTGS_CHECKS_VERSION_PATTERNS' ), true );
			if ( $new_patterns ) {
				$patterns = $new_patterns;
			}
		}
		if ( $this->shell->get_env( 'OTGS_CI_REPLACEMENTS' ) ) {
			$new_patterns = json_decode( $this->shell->get_env( 'OTGS_CI_REPLACEMENTS' ), true );
			if ( $new_patterns ) {
				$patterns = $new_patterns;
			}
		}

		/** @var RegExArguments $regex_args */
		foreach ( $patterns as $index => &$regex_args ) {
			$regex_args['matchingGroup'] = array_key_exists( 'matchingGroup', $regex_args ) ? $regex_args['matchingGroup'] : 'last';
		}

		return $patterns;
	}

	private function extractSemVer( $version ) {

		$replaced1 = str_replace( array( '-', '_', '+' ), '.', trim( $version ) );
		$replaced2 = preg_replace( '/([^0-9\.]+)/', '.$1.', $replaced1 );
		$replaced3 = str_replace( '..', '.', $replaced2 );
		$elements  = explode( '.', $replaced3 );

		$naked_elements = array( '0', '0', '0' );

		$elements_count = 0;
		foreach ( $elements as $element ) {
			if ( 4 === $elements_count || ! is_numeric( $element ) ) {
				break;
			}
			$naked_elements[ $elements_count ] = $element;
			$elements_count++;
		}

		return implode( '.', $naked_elements );
	}

	private function getMainPluginFile() {
		$files = scandir( $this->cwd );

		foreach ( $files as $file ) {
			if ( strpos( $file, '.' ) === 0 || is_dir( $this->cwd . '/' . $file ) ) {
				continue;
			}
			$this->current_file_content = $this->shell->read_file( $file );

			if ( $this->is_main_plugin_file() ) {
				return array(
					'file'    => $file,
					'content' => $this->current_file_content,
				);
			}
		}

		return null;
	}

	private function is_main_plugin_file() {
		$file_content = $this->current_file_content;

		$file_content = trim( preg_replace( '/\t|\n|\r/', '', $file_content ) );

		return strpos( $file_content, '<?php' ) === 0
			   && strpos( $file_content, 'Plugin Name: ' ) > 0
			   && strpos( $file_content, 'Description: ' ) > 0;
	}
}
