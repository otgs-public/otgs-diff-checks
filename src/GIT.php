<?php

namespace OTGS\DiffChecks;

class GIT {
	private $current_branch;
	private $files = [];
	/**
	 * @var \OTGS\DiffChecks\Shell
	 */
	private $shell;
	/**
	 * @var \OTGS\DiffChecks\Output
	 */
	private $output;

	public function __construct( Shell $shell, Output $output ) {
		$this->shell  = $shell;
		$this->output = $output;
	}

	public function getLastTag() {
		$command = 'git describe --abbrev=0';

		$diff_result = $this->shell->exec( $command );

		$tag = $diff_result->output[0];

		if ( $tag ) {
			return $tag;
		}

		$this->output->error( 'Could not find the last tag!' );
		$this->shell->stop( 100 );
	}

	function get_files( $target_branch, $extensions = [] ) {
		$this->init_changed_files( $target_branch );

		if ( $this->files && $extensions ) {
			return $this->filter_files( $this->sanitize_extensions( $extensions ) );
		}

		return [];
	}

	private function init_changed_files( $target_branch ) {
		if ( ! $this->files ) {

			$command = 'git diff --name-only --diff-filter=ACMR ' . $this->get_merge_base( $target_branch );

			$diff_result = $this->shell->exec( $command );

			$results     = $diff_result->output;
			$this->files = array_unique( $results );
		}
	}

	private function get_merge_base( $target_branch ) {
		return '$(git merge-base origin/' . $this->get_current_branch() . ' ' . $target_branch . ')';
	}

	function get_current_branch() {
		if ( ! $this->current_branch ) {
			if ( getenv( 'CI_MERGE_REQUEST_TARGET_BRANCH_NAME' ) ) {
				$this->current_branch = getenv( 'CI_MERGE_REQUEST_TARGET_BRANCH_NAME' );
			} else {
				$command       = "git branch | grep \* | cut -d ' ' -f2";
				$branch_result = $this->shell->exec( $command );

				$this->current_branch = $branch_result->output[0];
			}
		}

		return $this->current_branch;
	}

	/**
	 * @param $extensions
	 *
	 * @return array
	 */
	private function filter_files( $extensions ) {
		return array_values(
			array_filter( $this->files, function ( $item ) use ( $extensions ) {
				$file_parts = explode( '.', $item );

				return in_array( end( $file_parts ), $extensions, true );
			} )
		);
	}

	/**
	 * @param $extensions
	 *
	 * @return array
	 */
	private function sanitize_extensions( $extensions ) {
		return array_map( function ( $item ) {
			if ( strpos( $item, '.' ) === 0 ) {
				return substr( $item, 1 );
			}

			return $item;
		}, $extensions );
	}

	function build_diff( $target_branch ) {
		$diff_file = '/tmp/diff.txt';
		$command   = 'git diff ' . $this->get_merge_base( $target_branch ) . ' > ' . $diff_file;

		$build_diff_result = $this->shell->exec( $command );

		if ( $build_diff_result->return_var > 0 ) {
			$this->shell->stop( $build_diff_result->return_var );
		}

		return $diff_file;
	}
}
